﻿using UnityEngine;
using System.Collections;

namespace ScalAR.Utils
{
	public class ButtonBehaviour : MonoBehaviour
	{
		public string url;

		public void RedirectToUrl()
		{
			Application.OpenURL (url);
		}
	}
}