﻿using UnityEngine;
using UnityEditor;
using UnityEngine.UI;

using System.Collections.Generic;

using ScalAR.Utils;

[System.Serializable]
public class Codex
{
	[System.Serializable]
	public class Image
	{
		public string id;
		public string path;
	}
	[System.Serializable]
	public class Text
	{
		public string id;
		public string content;
	}
	[System.Serializable]
	public class Model
	{
		public string id;
		public string path;
	}
	[System.Serializable]
	public class Video
	{
		public string id;
		public string path;
	}
	[System.Serializable]
	public class Button
	{
		public string id;
		public string content;
		public string path;
		public string link;
	}
	[System.Serializable]
	public class Template
	{
		public string templateName;
		public string imageTarget;
		public Image[] images;
		public Text[] texts;
		public Model[] models;
		public Video[] videos;
		public Button[] buttons;
	}

	public Template[] templates;
}

public static class GenesisScript
{
	[MenuItem("Scalar/Generate Templates")]
	private static void GenerateTemplates()
	{
		Object imageTarget_Prefab = AssetDatabase.LoadAssetAtPath("Assets/Vuforia/Prefabs/ImageTarget.prefab", typeof(GameObject));

		GameObject temp, tempComponent = null, tmp = null;

		Texture2D textureHolder;

		Codex codexInstance = JsonUtility.FromJson<Codex> (Resources.Load<TextAsset>("Codex").text);

		Dictionary<string, GameObject> templateComponents = new Dictionary<string, GameObject> ();

		for(int i = 0 ; i < codexInstance.templates.Length ; i++)
		{
			Debug.Log (codexInstance.templates [i].templateName);
			temp = GameObject.Instantiate (imageTarget_Prefab as GameObject);
			temp.name = codexInstance.templates [i].imageTarget;

			temp = GameObject.Instantiate (Resources.Load ("Prefabs/" + codexInstance.templates [i].templateName) as GameObject, temp.transform) as GameObject;
			temp.name = temp.name.Replace ("(Clone)", "");

			templateComponents.Clear ();

			GetAllTemplateComponents (temp, templateComponents);

			foreach(KeyValuePair<string, GameObject> templateComponent in templateComponents)
			{
				Debug.Log (templateComponent.Key + " " + templateComponent.Value.name);
			}
			for(int j = 0 ; j < codexInstance.templates[i].images.Length ; j++)
			{
				templateComponents.TryGetValue (codexInstance.templates [i].images [j].id, out tempComponent);

				textureHolder = Resources.Load<Texture2D> ("Images/" + codexInstance.templates [i].images [j].path);

				tempComponent.GetComponent<Image> ().sprite = Sprite.Create (textureHolder, new Rect (0, 0, textureHolder.width, textureHolder.height), new Vector2 (0.5f, 0.5f));
			}
			for(int k = 0 ; k < codexInstance.templates[i].texts.Length ; k++)
			{
				templateComponents.TryGetValue (codexInstance.templates [i].texts [k].id, out tempComponent);
				tempComponent.GetComponent<Text> ().text = codexInstance.templates [i].texts [k].content;
			}
			for(int l = 0 ; l < codexInstance.templates[i].models.Length ; l++)
			{
				templateComponents.TryGetValue (codexInstance.templates [i].models [l].id, out tempComponent);
				tmp = GameObject.Instantiate(Resources.Load ("Models/" + codexInstance.templates [i].models [l].path), tempComponent.transform.position, tempComponent.transform.rotation, tempComponent.transform) as GameObject;
				tmp.name = tmp.name.Replace("(Clone)", "");
			}
			for(int m = 0 ; m < codexInstance.templates[i].videos.Length ; m++)
			{
				templateComponents.TryGetValue (codexInstance.templates [i].videos [m].id, out tempComponent);
				tempComponent.GetComponent<VideoPlaybackBehaviour> ().m_path = codexInstance.templates [i].videos [m].path;
			}
			for(int n = 0 ; n < codexInstance.templates[i].buttons.Length ; n++)
			{
				templateComponents.TryGetValue (codexInstance.templates [i].buttons [n].id, out tempComponent);
				tempComponent.transform.GetChild(0).GetComponent<Text> ().text = codexInstance.templates [i].buttons [n].content;

				tempComponent.GetComponent<ButtonBehaviour> ().url = codexInstance.templates [i].buttons [n].link;

				if(codexInstance.templates [i].buttons [n].path != null)
				{
					textureHolder = Resources.Load<Texture2D> ("Images/" + codexInstance.templates [i].buttons [n].path);

					tempComponent.GetComponent<Image> ().sprite = Sprite.Create (textureHolder, new Rect (0, 0, textureHolder.width, textureHolder.height), new Vector2 (0.5f, 0.5f));
				}
			}
		}
	}

	private static void GetAllTemplateComponents(GameObject currentTemplate, Dictionary<string, GameObject> templateComponents)
	{
		Transform[] allChildTransforms = currentTemplate.GetComponentsInChildren<Transform>();

		foreach(Transform child in allChildTransforms)
		{
			if(child.CompareTag("TemplateComponent"))
				templateComponents.Add (child.name, child.gameObject);
		}
	}
}